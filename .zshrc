# FILE: ~/.zshrc

powerline_source=/usr/share/powerline/bindings/zsh/powerline.zsh
syntax_highlighting_source=/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e
export EDITOR=vim


# PROMPT
# ======

# set variable debian_chroot if running in a chroot with /etc/debian_chroot
if [[ -z "$debian_chroot" ]] && [[ -r /etc/debian_chroot ]]; then
     debian_chroot=$(cat /etc/debian_chroot)
fi

setopt PROMPT_SUBST # allow for functions in the prompt.
autoload -U colors
autoload -U add-zsh-hook

if [ -f $powerline_source ]; then
	export VIRTUAL_ENV_DISABLE_PROMPT=1
	source $powerline_source
elif [ -f ~/.zsh/gitstatus.sh ]; then
	source ~/.zsh/gitstatus.sh
	export PROMPT='%K{blue}%n@%m%K{magenta}${debian_chroot:+($debian_chroot)}%k %B%F{cyan}%163<...<%~ %b$(git_super_status)
%}%F{white}%?%# %b%f%k'
#%}%F{white}%?%# %b%f%k'
	export ZSH_THEME_GIT_PROMPT_BRANCH="%{$fg_bold[green]%}"
	export ZSH_THEME_GIT_PROMPT_CHANGED="%{$fg[cyan]%}✚"
	# Local Status Symbols
	# ✔		repository clean
	# ●n	there are n staged files
	# ✖n	there are n unmerged files
	# ✚n	there are n changed but unstaged files
	# …		there are some untracked files
	# Branch Tracking Symbols
	# ↑n	ahead of remote by n commits
	# ↓n	behind remote by n commits
	# ↑n↓m	branches diverged, other by m commits, yours by n commits
else
	export PROMPT='%K{blue}%n@%m%K{magenta}${debian_chroot:+($debian_chroot)}%k %B%F{cyan}%163<...<%~ %b${vcs_info_msg_0_}
%}%F{white}%?%# %b%f%k'
	autoload -Uz vcs_info
	add-zsh-hook precmd vcs_info
	zstyle ':vcs_info:*' enable git bzr svn
	zstyle ':vcs_info:*' formats "(%B%F{green}%b%f%m%u%c)"
	#zstyle ':vcs_info:*' check-for-changes true
fi
unset powerline_source

# prompt markers (OSC-133;A)
add_prompt_markers() {
	print -Pn "\e]133;A\e\\"
}
add-zsh-hook precmd add_prompt_markers


# HISTORY
# =======
HISTSIZE=1000
SAVEHIST=10000
HISTFILE=~/.zsh/history
setopt INC_APPEND_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt HIST_NO_STORE
#setopt SHARE_HISTORY # share history between multiple shells


# OPTIONS
# =======
setopt NO_NOTIFY # do not notify when background job is done
unsetopt BEEP
unsetopt HUP # don't send SIGHUP to background processes when the shell exits
setopt LIST_PACKED # columns with different widths in completion list (i.e. smaller list)
setopt AUTO_CD
setopt INTERACTIVE_COMMENTS


# KEYBINDINGS
# ===========
autoload -Uz edit-command-line insert-composed-char
zle -N edit-command-line
zle -N insert-composed-char

bindkey '\C-e' edit-command-line
# digraphs (see `man zshcontrib` → insert-composed-char for a list
bindkey '^[k' insert-composed-char	# Esc-k

# backward kill word now stops at /
autoload -Uz select-word-style
select-word-style bash
#export WORDCHARS='*?_[]~=&;!#$%^(){}' # we take out the slash, period, angle brackets, dash here.

case $TERM in
  screen* | tmux* )
	# tmux with xterm-keys off:
	#bindkey '^[[D' emacs-backward-word	# Ctrl-Left
	#bindkey '^[[C' emacs-forward-word 	# Ctrl-Right
	# tmux witht xterm-keys on:
	bindkey '^[[1;5D' emacs-backward-word	# Ctrl-Left
	bindkey '^[[1;5C' emacs-forward-word 	# Ctrl-Right

	_shift_arrow_settings=(
		# tmux with xterm-keys on:
		left	'^[[1;2D'	backward-char
		right	'^[[1;2C'	forward-char
		up	'^[[1;2A'	up-line-or-history
		down	'^[[1;2B'	down-line-or-history
	) ;;
  rxvt*)
	bindkey '^[Od' emacs-backward-word	# Ctrl-Left
	bindkey '^[Oc' emacs-forward-word	# Ctrl-Right

	_shift_arrow_settings=(
		left	'^[[d'	backward-char
		right	'^[[c'	forward-char
		up	'^[[a'	up-line-or-history
		down	'^[[b'	down-line-or-history
	) ;;
esac

# shift selection: (kopiowanie zaznaczonego: M-y, wklejanie: C-y lub: M-p lub: M-P)
_shift-arrow() {
	((REGION_ACTIVE)) || zle set-mark-command
	zle $1
}
for key seq widget ($_shift_arrow_settings) {
	eval "_shift-$key() _shift-arrow $widget"
	zle -N _shift-$key
	bindkey $seq _shift-$key
}
unset _shift_arrow_settings key seq widget
copy-region-as-kill-deactivate-mark() {
	zle copy-region-as-kill
	zle set-mark-command -n -1
}
zle -N copy-region-as-kill-deactivate-mark
bindkey '^[y' copy-region-as-kill-deactivate-mark
bindkey '^[P' vi-put-before
bindkey '^[p' vi-put-after


# COMPLETION
# ==========
autoload -Uz compinit
compinit
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
if [ -r ~/.config/dircolors ]; then
	eval "$(dircolors -b ~/.config/dircolors)"
else
	eval "$(dircolors -b)"
fi
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
# Completing process IDs with menu selection:
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*'   force-list always
# cache slow functions
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
# Prevent CVS files/directories from being completed:
zstyle ':completion:*:(all-|)files' ignored-patterns '(|*/)CVS'
zstyle ':completion:*:cd:*' ignored-patterns '(*/)#CVS'

# apt install zsh-autosuggestions
if [ -f /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh ]; then
	source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
	ZSH_AUTOSUGGEST_USE_ASYNC=1
	ZSH_AUTOSUGGEST_ACCEPT_WIDGETS=end-of-line
	bindkey '^ ' autosuggest-accept
fi
# apt install fzf  # CTRL-r, CTRL-t, ALT-c
if [ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]; then
	source /usr/share/doc/fzf/examples/key-bindings.zsh

	if [ -f ~/.zsh/forgit/forgit.plugin.zsh ]; then
		source ~/.zsh/forgit/forgit.plugin.zsh
	fi
fi

# ENV
# ===
export PATH=~/.local/bin:$PATH:/usr/sbin:/sbin
if [ -f ~/.local/bin/pager ]; then
	export PAGER=~/.local/bin/pager
else
	export PAGER='less -R'
fi

#export MANPAGER="/bin/sh -c \"col -b | vim -c 'set ft=man ts=8 nomod nolist nonu noma' -\""
export MANPAGER='nvim +Man! +set\ showtabline=1\ laststatus=1'

# apt install pspg
if [ "$(command -v pspg)" ]; then
	export PSQL_PAGER='pspg -s 17'
fi

# apt install bat
if [ "$(command -v batcat)" ]; then
	alias cat='PAGER=less batcat'
fi
export LC_CTYPE=pl_PL.UTF-8


# ALIASES
# =======
# suffix based:
#alias -s py='ipython3 -i'
alias -s jpg=xdg-open
alias -s jpeg=xdg-open
alias -s dsc='dpkg-source -x'
alias -s deb='sudo apt install'
alias -s changes='sudo debi'
alias -s tar.xz='tar Jxf'
alias -s tar.bz2='tar jxf'
alias -s tar.gz='tar zxf'

# global:
alias -g G="| grep"
alias -g L="| $PAGER"
alias -g ND='*(/om[1])' # newest directory
alias -g NF='*(.om[1])' # newest file

# apps:
alias i=ipython3
alias a=aptitude
alias b='sudo btrfs'
alias v=vcsh
alias ip='ip -color=auto'
alias afind='ag -il'
alias mbts='bts --mailreader='\''neomutt -f %s'\'' --mbox show'
alias gbp-diff='git diff debian/`dpkg-parsechangelog -S Version -c 2 -o 1`..'
alias rsync-moje='rsync --archive --update --partial --progress --acls --xattrs --hard-links --devices --specials --exclude .snapshots --exclude .cache'
alias rsync-moje-rmdir='rsync-moje --prune-empty-dirs'
alias dd-moje='dd bs=1M conv=fdatasync status=progress'
alias yta='youtube-dl --xattrs --add-metadata --extract-audio'

# apt install eza
if [ "$(command -v eza)" ]; then
  alias ls='eza --group-directories-first -G --color auto -a -s type'
  alias ll='eza --group-directories-first --group --time-style=long-iso --color auto -l --icons -a'
# apt install exa
elif [ "$(command -v exa)" ]; then
  alias ls='exa --group-directories-first -G --color auto -a -s type'
  alias ll='exa --group-directories-first --group --time-style=long-iso --color auto -l --icons -a'
else
  alias ls="ls --group-directories-first --color=auto"
  alias ll="ls -lAv --group-directories-first --color=auto --time-style='+%Y-%m-%dT%H:%M:%S%z'"
fi

# other
#alias showmyip='wget -q http://ip.dnsexit.com -O - | tail -n 1'
alias showmyip='curl ifconfig.co'
alias pogoda='f(){ curl --silent -H "Accept-Language: pl" "http://v2.wttr.in/$1" | head -n -2; unset -f f; }; f'
alias pogoda2='f(){ curl --silent -H "Accept-Language: pl" "http://wttr.in/$1" | head -n -2; unset -f f; }; f'
alias show-nearest-opennic='wget -q -O - "https://api.opennicproject.org/geoip/?res=3&ip=&nearest"'
alias show-nearest-opennic-json='wget -q -O - "https://api.opennicproject.org/geoip/?json&res=3&ip=&nearest"'

# hash:
hash -d log=/var/log/
hash -d workspace=${HOME}/workspace/

# make dir and cd into it
md() {mkdir -p "$*" && cd "$*"}

kolorki() {
	TEXT=$1
	[ "$TEXT" != '' ] || TEXT=Test
	for code in $(seq -w 0 255); do
		for attr in 0 1; do
			printf "%s-%03s %b$TEXT%b\n" "${attr}" "${code}" "\e[${attr};38;05;${code}m" "\e[m";
	       	done;
	done | column -c $((COLUMNS*2))
}

# unmap ctrl-s
stty stop undef


# LOOK:
# =====
# apt install zsh-syntax-highlighting
if [ -f $syntax_highlighting_source ]; then
	# keep it at the end of .zshrc (after ZLE definitions, etc.)
	source $syntax_highlighting_source
	ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
	ZSH_HIGHLIGHT_PATTERNS+=('sudo' 'fg=white,bold,bg=red')
	ZSH_HIGHLIGHT_STYLES[cursor]='bg=blue'
	ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=063'
	ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=060'
	ZSH_HIGHLIGHT_STYLES[commandseparator]='fg=051'
	ZSH_HIGHLIGHT_STYLES[redirection]='fg=051'
	ZSH_HIGHLIGHT_STYLES[alias]='fg=041'
	ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=046'
	ZSH_HIGHLIGHT_STYLES[function]='fg=083'
	ZSH_HIGHLIGHT_STYLES[path]='fg=011'
	ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=059,bold'
	ZSH_HIGHLIGHT_STYLES[globbing]='fg=blue,bold'
	ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=magenta'
	ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=011'
	ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=154'
	ZSH_HIGHLIGHT_STYLES[assign]='fg=045'
	ZSH_HIGHLIGHT_STYLES[comment]='fg=007'
fi
unset syntax_highlighting_source


# LOCAL CONFIG
# ============
if [ -r ~/.zsh/local ]; then
	source ~/.zsh/local
fi
